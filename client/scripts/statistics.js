/**
 * Statistics page frontend scripts for backswing
 *
 * (C) 2016 Richard Vogel
 */
if (Meteor.isClient) {
    var calcRep = new CalculationRepository (Calculations),
        targetCalcRep = new TargetCategoryRepository (TargetCategories)
    ;

    Template.statistics.rendered = function () {
        $('.input-daterange input').each(function() {

            $(this).datepicker({
                language: 'de',
                maxViewMode: 1,
                startDate: calcRep.getFirstCalculationDate (),
                todayHighlight: true,
                autoclose: true
            });
        });

        $('#dateFrom').datepicker ('setDate', calcRep.getFirstCalculationDate ());
        $('#dateTo').datepicker ('setDate', calcRep.getLatestCalculationDate ());

    };


    Template.statistics.helpers ({
        getTargetCategories: function () {
            return targetCalcRep.findAllActive ();
        },
        getFilterCalcCount: function () {
            return Session.get ('amountOfCalcs');
        },
        getFilterSumOfCalcValues: function () {
            return Session.get ('amountOfCalcs');
        },
        getFilterSumOfCalcItems: function () {
            return Session.get ('sumOfCalcItems');
        },
        getFilterCalculationProfit: function () {
            return Session.get ('calcProfit');
        },
        getCostsByTarget: function () {
            return Session.get ('costsByTarget');
        }
    });

    Template.statistics.events ({
        'submit #filterForm': function (evt) {
            evt.preventDefault ();
            var $target = $(evt.target),
                calcStatusVal = $target.find ('input[name=calculationState]:checked').val (),
                checkedTargetCats = $target.find ('input.target-categories:checked').map (function () {
                    return $(this).val ();
                }).get (),
                startTime = $('#dateFrom').datepicker ('getDate'),
                endTime = $('#dateTo').datepicker ('getDate')
            ;

            //build query constraints
            var constraints = {};

            if (calcStatusVal === 'opened') {
                constraints.isClosed = false;
            } else if (calcStatusVal === 'closed') {
                constraints.isClosed = true;
            }

            constraints.createdAt = {
                $gte: startTime,
                $lte: endTime
            };

            constraints.category = {$in : checkedTargetCats};

            var calcs = calcRep.findByConstraints (constraints),
                amountOfCalcs = calcs.length,
                sumOfValues = 0,
                sumOfItems = 0,
                barChartData = {
                    labels: [],
                    datasets: []
                },
                availableCalcCats = _.keys (calculationCategoryTypes),
                costsByTarget = {}
                ;

            _.each (availableCalcCats, function (availableCat) {
                costsByTarget[availableCat] = {
                    'label':  backswing.language.translate ('calculation_category_'+availableCat),
                    'value': 0
                };
            });

            //Create some numbers
            _.each (calcs, function (calc) {
                /** @var {CalculationModel} calc */
                sumOfValues += calc.getValue ();
                sumOfItems += calc.calculateItemValue (_.keys (calculationCategoryTypes));


                //Sum up items
                _.each (calc.getItems (), function (item) {
                    var catType = item.getCalculationCategory ();


                    /** @var {CalculationItem} item  */
                    if (_.contains (availableCalcCats, catType)) {

                        if (!_.contains (costsByTarget, catType)) {
                            costsByTarget[catType] = {
                                'label':  backswing.language.translate ('calculation_category_'+catType),
                                'value': 0
                            }
                        }

                        costsByTarget[catType]['value'] += (new Number(item.calculatePrice ()));

                    }
                });

                /*
                var calcCreateDate = calc.createdAt,
                    calcMoment = moment (calcCreateDate),
                    year = calcMoment.year () +'',
                    month = calcMoment.month () +'',
                    barLabel = year + ' - ' + month,
                    dataPos = -1

                ;

                var availableBarLabels = barChartData.labels;
                if (!availableBarLabels[barLabel]) {
                    availableBarLabels.push (barLabel);
                    dataPos = availableBarLabels.length - 1;
                    barChartData.datasets.push ()
                } else {
                    dataPos = availableBarLabels.indexOf (barLabel)
                }

                var dataContainer = barChartData.dataSet
                */

            });

            //Make it a common Array again or meteor cannot handle it
            var costArr = _.map (costsByTarget, function (val) {
                return val;
            });

            Session.set ('costsByTarget', costArr);
            Session.set ('amountOfCalcs', amountOfCalcs);
            Session.set ('sumOfCalcValues', sumOfValues);
            Session.set ('sumOfCalcItems', sumOfItems);
            Session.set ('calcProfit', sumOfValues - sumOfItems);
        }
    })
}