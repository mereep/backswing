/**
 * Global namespace
 * @type {{}}
 */
//Some startup code to init needed objects (SERVER + CLIENT)

backswing = {};
backswing.userHelper = new UserHelper();
backswing.language = new Language();
backswing.language.addLanguage('de', langDE);
backswing.language.addLanguage('default', langDefault);

//##CLIENT
if (Meteor.isClient) {
  accountsUIBootstrap3.setLanguage(backswing.userHelper.getUserLanguage());
  var UserRepository = new window.UserRepository(Meteor.users);

  Template.registerHelper ('translate', function (id) {
      return backswing.language.translate(id);
    });

  Template.registerHelper ('setTitle', function (title) {
      document.title = title + ' - ' + backswing.language.translate('appName');
    });

  Template.registerHelper('getIdForEntity', function (entity) {
    if(typeof entity._id === 'string') {
      return entity._id;
    } else {
      return undefined;
    }
  });

  Template.registerHelper ('toUserDate', function (date) {
    var dateFormatString = backswing.language.translate('common_date_complete')
        ;
    return moment(date).format(dateFormatString);
  });

  Template.registerHelper ('getUserNameFromId', function (userId) {
    check (userId, String);
    var usr = UserRepository.findById (userId);
    if (typeof usr === 'object') {
      return usr.getName();
    }

    return undefined;

  });

  Template.registerHelper ('getNameForUserId', function (userId) {
    check (userId, String);
    var user = UserRepository.findById (userId);
    check (user, Object);
    return user.getName ();

  });


  Template.registerHelper('isAnotherUserIdThanCurrent', function (userId) {
    return ! (Meteor.userId() === userId);
  });

  Template.registerHelper('isOwnUserId', function (userId) {
    return !! (Meteor.userId() === userId);
  });

  Template.registerHelper ('toFixed', function (number) {
      return (new Number (number)).toFixed (2);
  });

  Template.body.events({
    'click .nach-oben': function (event) {

      event.preventDefault ();
      $('body').scrollTop (0);
    },

    'scroll': function () {
      var scrollPos  = $ ('body').scrollTop ();

      if (scrollPos > 200) {
        $('.to-top-container').addClass ('visible');
      } else {
        $('.to-top-container').removeClass ('visible');

      }

    }
  })

}


//####SERVER
if (Meteor.isServer) {

  Meteor.methods({
    translate: function(id){
      return backswing.language.translate(id);
    },

  });


  Meteor.startup(function () {

    var catRepo = new CalculationCategoryRepository(CalculationCategories);
    var targetCatRepo = new TargetCategoryRepository(TargetCategories);
    var calculationsRepo = new CalculationRepository(Calculations);

    //Generate Dummies if not existent
    _.each (calculationCategoryTypes, function (type) {
      if(CalculationCategories.find({type: type}).count() < 1){
        catRepo.addDummyFor(type);
      }
    });

    _.each (targetCategoryTypes, function (type) {
      if(TargetCategories.find({type: type}).count() < 1){
        targetCatRepo.addDummyFor(type);
      }
    });

    calculationsRepo.fillTo(100);
  });
}
