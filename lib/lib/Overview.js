/**
 * Created by birne on 27.11.15.
 */
if(Meteor.isClient) {
    Meteor.subscribe('TargetCategories');
    Meteor.subscribe('Calculations');
    Meteor.subscribe('AllUsers');

    var TargetCategoryRepository = new window.TargetCategoryRepository(window.TargetCategories);
    var CalculationRep = new window.CalculationRepository(window.Calculations);
    var UserRepository = new window.UserRepository(Meteor.users);

    Template.overview.events({
        'click .tab': function (evt) {
            var id = evt.currentTarget.id;
            Session.setPersistent('view.overview.chosenTab', id);
        },

        'click .calculation': function () {
            var calcId = this._id;
            Router.go('/calculation/show/'+calcId);
        },

        'click .delete': function (evt, template) {
            evt.preventDefault ();
            evt.stopPropagation ();

            var calcId = $(evt.currentTarget).attr ('data-calculation-id');

            sweetAlert(
                {
                    title: backswing.language.translate ('common_are_you_sure'),
                    text: backswing.language.translate ('overview_really_delete'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: backswing.language.translate ('common_yes'),
                    cancelButtonText: backswing.language.translate ('common_no'),
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        Meteor.call ('deleteCalculation', calcId, function (err, msg) {
                            if (!err) {
                                Notifier.success (msg, true);
                            } else {
                                Notifier.error (err.toString(), true);
                            }
                        });
                    }
                }

            )
        }
    });
    Template.overview.helpers({
        categories: function () {
            return TargetCategoryRepository.findAllActive();
        },


        /**
         * Translates the label of targetCategoryModel
         *
         * @param {TargetCategoryModel} tc
         * @returns string
         */
        getTranslatedLabelForTargetCategory: function (tc) {
            return  backswing.language.translate('target_category_'+tc.getLabelId());
        },
        getLabelForTargetCategory: function (tc) {
          return tc.getLabelId();
        },

        getCalculationsCountForCategory: function (tc) {
            return CalculationRep.getCalculationsCountByCategory(tc.getType());
        },

        getOpenCalculationCountForCategory: function (tc) {
            return CalculationRep.getOpenCalculationsCountByCategory(tc.getType());
        },
        /**
         * Returns true if we have open calculations
         * @param {TargetCategoryModel} tc
         */
        hasOpenCalculations: function (tc) {
            return CalculationRep.getOpenCalculationsCountByCategory(tc.getType()) > 0;

        },

        getTitleForCalculation: function (category) {
            return category.getTitle();
        },

        getCalculationsByTargetCategory: function (tc) {
            var page = Session.get('overview.pagination.'+tc.getType()) || 0,
                pageSize = 5,
                maxEntries = CalculationRep.getCalculationsCountByCategory(tc.getType());

            if(pageSize * page > maxEntries) {
                Session.set('overview.pagination'+pagination, 0);
            }

            return CalculationRep.getAllCalculationsByCategory(tc.getType(), page * pageSize, pageSize);
        },

        getLastModifiedForCalculation: function (calculation) {
            var dateFormatString = backswing.language.translate('common_date_complete'),
                date = calculation.getLastModified();

            return moment(date).format(dateFormatString);
        },

        /**
         * Gets all names that confirmed this calculation
         * @return array
         */
        getAccpetorsForCalculation: function (calculation) {
            var acceptors = calculation.getAcceptedBy(),
                names = [];

            if(typeof acceptors === 'object' && acceptors.length >= 1) {
                _.each(acceptors, function (acceptor) {
                    var usr = UserRepository.findById(acceptor);
                    if (typeof usr === 'object') {
                        names.push (usr.getName());
                    }
                });
            }
            return names;
        },

        /**
         * Checks if calculation is closed
         * @param calculation
         * @returns {boolean}
         */
        calculationIsClosed: function (calculation) {
            return calculation.getIsClosed ();
        },

        /**
         * Returns if the current user should accept this calculation
         *
         * @param calculation
         * @returns {boolean}
         */
        shouldAcceptCalculation: function (calculation) {
            if(_.contains(calculation.getAcceptedBy(), Meteor.userId())) {
                return false;
            } else {
                if(_.contains (calculation.getNeedsAcceptanceFrom (), Meteor.userId())) {
                    return true;
                }
            }

            return false;
        },

        noCalculationsForTargetCategory: function (cat) {
            return CalculationRep.getCalculationsCountByCategory(cat.getType()) <= 0;
        }

    });

    Template.overview.onRendered(function () {

        //Preselect old tab
        if(Session.get('view.overview.chosenTab')){
            $(document).ready(function () {
                jQuery('#'+Session.get('view.overview.chosenTab')).tab('show');
            });
        }else {
            jQuery('.categories .tab').first().tab('show');

        }
    })
}

if (Meteor.isServer) {

    Meteor.methods ({
        'deleteCalculation': function (calcId) {
            if (Match.test (calcId, String)) {
                var calcRep = new CalculationRepository (Calculations),
                    calc = calcRep.findById (calcId)
                ;

                if (Match.test (calc, CalculationModel)) {
                    if (calcRep.deleteCalculation (calcId)) {
                        return backswing.language.translate ('overview_calculation_delete_success');
                    } else {
                        throw new Meteor.Error (3453454367, backswing.language.translate ('overview_calculation_delete_db_error'));
                    }
                } else {
                    throw new Meteor.Error (4322365987, backswing.language.translate ('common_internal_error'));
                }
            } else {
                throw new Meteor.Error (645645654, backswing.language.translate ('common_internal_error'));
            }
        }
    });

    Meteor.publish('TargetCategories', function () {
        return TargetCategories.find();
    });

    Meteor.publish('Calculations', function () {
        return Calculations.find ();
    });

    Meteor.publish('AllUsers', function () {
        var users =  Meteor.users.find({}, {'fields': {profile:1 ,role:1}});
        return users;
    });
}