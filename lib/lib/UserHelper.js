/**
 * (C) 2015 Richard Vogel
 * E-Mail: webdes87@gmail.com
 *
 * Created 2015-11-23
 * This file belongs to the backswing meteor package
 *
 * Helper component for user specific issues
 **/


UserHelper = function () {
    this.getUserLanguage = function(){
      return 'de';
    };

    this.isOwnUserId = function (userId) {
        return Meteor.userId() === userId;
    };

    /**
     * Returns name of user or ''
     * @param {string} id
     * @return string
     */
    this.getUserNameById = function (id) {

    };


    /**
     * Checks if calculationItems category can be edited by currently logged in user
     *
     * @param {CalculationItem} calculationItem
     * @returns {boolean}
     */
    this.checkIfUserCanEditCalculationItem = function (calculationItem) {
        var user = Meteor.user ();

        if (user) {
            if (typeof calculationItem === 'object') {
                var calculationCatRep = new CalculationCategoryRepository (CalculationCategories);
                var userRole = user.role;
                var calculationItemCategoryType = calculationItem.calculationCategory;

                /** @var CalculationCategoryModel calcCat*/
                var calcCat = calculationCatRep.findByType (calculationItemCategoryType);

                if (Match.test (calcCat, CalculationCategoryModel) && calcCat.isActive() && calcCat.isEditableByRole(userRole)) {
                    return true;
                }
            }
        }
        return false;
    };

    /**
     * Checks if the current logged in user is allowed to edit items within this category
     * (kitchen workers are not allowed to add items to sourcing etc)
     * @param {CalculationCategoryModel} calcCat
     */
    this.checkIfUserCanEditCalculationCategory = function (calcCat) {
        check (calcCat, CalculationCategoryModel);
        var user = Meteor.user ();

        if (user) {
            var userRole = user.role;
            if (calcCat.isActive() && calcCat.isEditableByRole(userRole)) {
                return true;
            }
        }

        return false;
    }
};