Notifier =  {

    /**
    * Throw Meteor error and inform user (if informUser equals true)
    *
    * @param {string} msg
    * @param {boolean} informUser
    */
    error: function (msg, informUser) {
        check(msg, String);
        check(informUser, Boolean);

        if(informUser === true) {
          Notifications.error(backswing.language.translate('error_error'), msg);
        }

        Meteor.Error(msg);
    },

    success: function (msg, informUser) {
        check(msg, String);
        check(informUser, Boolean);

        if(informUser === true) {
            Notifications.success(backswing.language.translate('success_success'), msg);
        }

    }
};