CalculationCategories = new Mongo.Collection('calculation_categories');

CalculationCategoryModel = function(){
    this.createdAt = new Date();
    this.lastModified= new Date();

    this.type  = '';
    this.label = '';
    this.order = '';
    this.active = true;

    this.editableByRoles = [];
};

CalculationCategoryModel.prototype = {
    /**
     * Sets the type of this category
     * @param type
     */
    setType: function(type){
        check(type, String);
        this.type = type;
    },

    /**
     * Gets the type
     * @returns {*|string|string|string}
     */
    getType: function () {
        return this.type;
    },

    /**
     * Sets if category is active
     * @param {boolean} active
     */
    setActive: function (active) {
        check(active, Boolean);
        this.active = active;
    },

    /**
     * Checks if val is active
     * @returns {boolean}
     */
    isActive: function () {
        return this.active === true;
    },


    /**
     * Sets the order of category for view
     *
     * @param {int} order
     */
    setOrder: function(order) {
        check(order, Match.Integer);
        this.order = order;
    },

    /**
     * Sets the label for this entity
     *
     * @param {string} label
     */
    setLabel: function (label) {
        check(label, String);
        this.label = label;
    },

    /**
     * Gets label (for use with translation)
     * @returns {string}
     */
    getLabel: function () {
        return this.label;
    },

    /**
     * Sets the roles this category is editable by
     * @param array roles
     */
    setEditableByRoles: function(roles) {
        check(roles, [String]);
        this.editableByRoles = roles;
    },

    /**
     * Checks against which roles this entity is editable
     *
     * @param roles
     * @returns array
     */
    getEditiableByRoles: function() {
        return this.editableByRoles;
    },

    /**
     * Checks if given role is in editableByRoles array
     *
     * @param {string} role
     * @returns {boolean}
     */
    isEditableByRole: function (role) {
        check (role, String);

        if (_.contains(this.getEditiableByRoles(), role)) {
            return true;
        } else {
            return false;
        }
    }

};

CalculationCategoryRepository = function (collection) {
    var _collection = collection,
        _targetDummy = CalculationCategoryModel
    ;

    /**
     * Adds a dummy into repository for collection
     * @param type
     */
    this.addDummyFor = function(type) {
        check(type, String);

        var dummyCat = new CalculationCategoryModel();

        var order = 10000, editableByRoles = [], label='unknown';
        switch (type) {
            case calculationCategoryTypes.sourcing:
                order = 10;
                editableByRoles = [userRoles.admin, userRoles.kitchen];
                label = 'calculation_category_'+calculationCategoryTypes.sourcing;
                break;
            case calculationCategoryTypes.staff:
                order = 20;
                editableByRoles = [userRoles.admin, userRoles.kitchen];
                label = 'calculation_category_'+calculationCategoryTypes.staff;
                break;
            case calculationCategoryTypes.transport:
                order = 30;
                editableByRoles = [userRoles.admin];
                label = 'calculation_category_'+calculationCategoryTypes.transport;
                break;
            case calculationCategoryTypes.facility:
                order = 40;
                editableByRoles = [userRoles.admin];
                label = 'calculation_category_'+calculationCategoryTypes.facility;
                break;
            case calculationCategoryTypes.overhead:
                order = 50;
                editableByRoles = [userRoles.admin];
                label = 'calculation_category_'+calculationCategoryTypes.overhead;
                break;
            case calculationCategoryTypes.other:
                order = 30;
                editableByRoles = [userRoles.admin];
                label = 'calculation_category_'+calculationCategoryTypes.other;
                break;
            default:
                Meteor.Error(32984902380923, 'Calculation type '+type+ 'not specified (Code: 32984902380923)');
        }

        dummyCat.setActive(true);
        dummyCat.setLabel(label);
        dummyCat.setOrder(order);
        dummyCat.setEditableByRoles(editableByRoles);
        dummyCat.setType(type);

        collection.insert(dummyCat);
    };

    this.getAllActive = function () {
        return this.mapAll(_collection.find({'active': true}).fetch(), _targetDummy)
    };

    this.findByType = function (type) {
        check (type, String);

        return this.map (_collection.findOne ({'type': type}), _targetDummy);
    };
};


CalculationCategoryRepository.prototype = AbstractRepository.prototype;