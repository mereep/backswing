/**
 * Created by birne on 03.12.15.
 */
var UserModel = function () {
    this.role = "none";
    this.emails = [];
    this.profile = {
        'name': ''
    };
};


UserModel.prototype = {
    /**
     * Gets the full name of user
     * @returns {string}
     */
    getName: function () {
        return this.profile.name
    },

    /**
     * Gets list of emails (if available only)
     */
    getEmails: function () {
        return this.emails;
    },

    /**
     * @see userRoles
     * @returns {string}
     */
    getRole: function () {
        return this.role;
    },

    /**
     * Returns true if this user is allowed the be responsible for a calculations
     *
     * @returns {boolean}
     */
    canBeResponsibleForCalculation: function () {
        if (this.getRole () === userRoles.admin || this.getRole () === userRoles.kitchen) {
            return true;
        }

        return false;
    }
};

UserRepository = function (collection) {
    var _collection = collection;

    /**
     * Return user record by name
     * @returns {*}
     */
    this.findById = function (id) {
        var usr = _collection.findOne({'_id': id});
        return this.map(usr, UserModel);
    };

    /**
     * Gets all users in database
     * @returns {*}
     */
    this.findAll = function () {
        return this.mapAll(_collection.find({_id: {'$ne':undefined}}).fetch(), UserModel);
    }
};

UserRepository.prototype = AbstractRepository.prototype;