/**
 * Created by birne on 24.11.15.
 */

TargetCategories = new Mongo.Collection('target_categories');

function TargetCategoryModel () {
    this.createdAt = new Date();
    this.lastModified = new Date();

    this.labelId = '';
    this.type = '';
    this.active = true;
};

TargetCategoryModel.prototype = {
    /**
     * Set the label for category (to translate)
     * @param label
     */
    setLabelId: function(label){
        check(label, String);
        this.labelId = label;
    },

    /**
     * Set the label for category (to translate)
     * @param {string} type
     */
    setType: function(type){
        check(type, String);

        this.type = type;
    },

    /**
     * Gets translateable label
     * @returns {string}
     */
    getLabelId: function () {
        return this.labelId;
    },

    /**
     * Gets the type of the category
     * @return {string}
     */
    getType: function () {
        return this.type;
    },

    setActive: function (active) {
        check(active, Boolean);
        this.active = active;
    },

    /**
     * @returns {boolean}
     */
    getActive: function () {
        return this.active;
    },

    translateLabel: function () {
        var lbl = this.getLabelId ();
        return backswing.language.translate ('target_category_'+lbl);
    }
};


TargetCategoryRepository = function (collection) {

    var _collection = collection,
        _targetDummy = TargetCategoryModel;


    this.addDummyFor = function (type) {
        check (type, String);
        var label = 'unknown';

        switch (type) {
            case targetCategoryTypes.golf:
                label = targetCategoryTypes.golf;
                break;

            case targetCategoryTypes.restaurant:
                label = targetCategoryTypes.restaurant;
                break;

            case targetCategoryTypes.film:
                label = targetCategoryTypes.film;
                break;

            case targetCategoryTypes.asyl:
                label = targetCategoryTypes.asyl;
                break;

            case targetCategoryTypes.other:
                label = targetCategoryTypes.other;
                break;

            default:
                Meteor.Error(328409238432, 'Target Category type '+type+ 'not specified (Code: 328409238432)');
                break;
        }

        var obj = new TargetCategoryModel();

        obj.setLabelId(label);
        obj.setType(type);
        obj.setActive(true);

        _collection.insert(obj);
    };


    this.findAllActive = function () {
        return this.mapAll(_collection.find({'active': true}).fetch(), _targetDummy);
    };

    /**
     * Gets exactly one element by id and maps
     * if none found return undefined
     *
     * @param id
     * @returns {TargetCategoryModel|undefined}
     */
    this.findById = function (id) {
        check(id, String);

        var elem = _collection.findOne({'_id': id});

        if (typeof elem === 'object') {
            return this.map(elem, _targetDummy);
        } else {
            return undefined;
        }
    }

};

TargetCategoryRepository.prototype = AbstractRepository.prototype;