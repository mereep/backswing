//This is meant embedded into CalculationModel.items

CalculationItem = function () {
    this._id = (new Mongo.ObjectID ()).valueOf();
    this.createdAt = new Date();
    this.lastModified = new Date();
    this.title = '';
    this.amount = 1;
    this.unitPrice = 0;
    this.type = calculationItemTypes.common; //This may influence the behaviour of display
    this.calculationCategory = calculationCategoryTypes.common; //For which calculation part this entry should be for (Staff, Cars, ....)
    this.comment = '';
};


CalculationItem.prototype = {
    /**
     * @returns {String}
     */
    getComment: function () {
        return this.comment;
    },
    /**
     *
     * @returns {number}
     */
    getUnitPrice: function () {
        return this.unitPrice;
    },

    /**
     *
     * @returns {*}
     */
    getTitle: function () {
        return this.title;
    },

    /**
     * Type is one of calculationItemTypes
     * @returns {*}
     */
    getType: function () {
        return this.type;
    },

    /**
     *
     * @returns {number}
     */
    getAmount: function () {
        return this.amount;
    },

    /**
     * Category is one of
     * @see calculationCategoryTypes
     *
     * @returns {string}
     */
    getCalculationCategory: function () {
        return this.calculationCategory;
    },

    /**
     * Calculates the current price by amount * unitPrice
     * @return {number}
     */
    calculatePrice: function () {
        return this.getUnitPrice () * this.getAmount ();
    }
    ,
    /**
     *
     * @param {string} title
     */
    setTitle: function (title) {
        check (title, String);
        this.title = title;
    },

    /**
     * Sets type of this entry (may have some different behaviour depending on type)
     *
     * @see calculationItemTypes
     * @param {string} type
     */
    setType: function (type) {
        check (type, String);
        this.type = type;
    },

    /**
     *
     * @param {number} amount
     */
    setAmount: function (amount) {
        check (amount, Number);
        this.amount = amount;
    },


    /**
     *
     * @param {number} up
     */
    setUnitPrice: function (up) {
        check (up, Number);
        this.unitPrice = up;
    },

    /**
     * Sets the type of this item
     *
     * @see calculationCategoryTypes
     * @param {String} calculationCategory
     */
    setCalculationCategory: function (calculationCategory) {
        check (calculationCategory, String);
        this.calculationCategory = calculationCategory;
    },

    /**
     *
     * @param {String} comment
     */
    setComment: function (comment) {
        check (comment, String);
        this.comment = comment;
    }




};