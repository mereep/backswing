AbstractRepository = function () {


};

AbstractRepository.prototype = {

    map: function (sourceObject, targetDummy) {
        if (sourceObject) {
            var obj = new targetDummy();
            _.each(sourceObject, function (value, index) {
                var setterName = 'set'+ s.capitalize(index);

                if (typeof obj[setterName] === 'function') {
                    obj[setterName].call (obj, value);
                } else {
                    obj[index] = value;
                }
            });

            return obj;
        } else {
            return undefined;
        }

    },

    mapAll: function (sourceObjects, targetObjectDummy) {
        var me = this;
        return _.map(sourceObjects, function (value) {
            var mapped = me.map(value, targetObjectDummy);
            return mapped;

        }); 
    },
    /**
     * Gets calls get id or returns property _id
     *
     * @param obj
     */
    getIdFor: function (obj) {
        if(typeof obj['getId'] === 'function') {
            return obj['getId'].call(obj);
        } else {
            return obj['_id'];
        }
    }
};
