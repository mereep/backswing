calculationItemTypes = {
    common:         'common',
    staff:          'staff',
};

calculationCategoryTypes = {
    sourcing:       'sourcing',
    transport:      'transport',
    staff:          'staff',
    facility:       'facility',
    overhead:       'overhead',
    other:          'other'
};

userRoles = {
    'admin':        'admin',
    'kitchen':      'kitchen'
};

targetCategoryTypes = {
    'golf':         'golf',
    'asyl':         'asyl',
    'restaurant':   'restaurant',
    'film':         'film',
    'other':        'other',

};

logTypes = {
    calculationCreated: 'calculation_created',
    calculationModified: 'calculation_modified',
    calculationClosed: 'calculation_closed',
    calculationDeleted: 'calculation_deleted',
    calculationAcceptanceChange: 'calculation_acceptance_change',
    calculationOpened: 'calculation_opened'
};

logTargets = ['calculation_show', 'calculation_overview'];