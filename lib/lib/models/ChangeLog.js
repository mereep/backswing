/**
 * Created by birne on 14.01.16.
 * This model represents a change entry to any database model
 * It has a record from the initiating user.
 *
 * May also be used to recover Data
 */
ChangeLog = new Mongo.Collection('change_log');

var ChangeLogModel = function () {
    this.createdAt = new Date();
    this.user = undefined;
    this.from = {};
    this.to = {};
    this.targets = [];

    /**
     * @see logTypes
     * @type {string}
     */
    this.logType = '';

    /**
     *
     * @param {string} user
     */
    this.setUser = function (user) {
        check (user, String);
        this.user = user;
    };

    /**
     *
     * @param {Array} targets
     */
    this.setTargets = function (targets) {
        check (targets, Array);
        this.targets = targets;
    };

    /**
     *
     * @param {object} from
     */
    this.setFrom = function (from) {
        check (from, Object);
        this.from = from;
    };

    /**
     *
     * @param {object} to
     */
    this.setTo = function (to) {
        check (to, Object);
        this.to = to;
    };

    /**
     *
     * @param {object} logType
     */
    this.setLogType = function (logType) {
        check (logType, String);
        this.logType = logType;
    };

    /**
     *
     * @returns {{}}
     */
    this.getTo = function () {
        return this.to;
    };

    /**
     *
     * @returns {{}}
     */
    this.getFrom = function () {
        return this.from;
    };

    /**
     *
     * @returns {String}
     */
    this.getUser = function () {
        return this.user;
    };

    /**
     *
     * @returns {string}
     */
    this.getLogType = function () {
        return this.logType;
    };

    /**
     *
     * @returns {Array}
     */
    this.getTargets = function () {
        return this.targets;
    };

    /**
     * Adds a target (if not already in)
     *
     * @param {string} targetId
     */
    this.addTarget = function (targetId) {
        var targets = this.getTargets();

        if (!_.contains (targets, targetId)) {
            this.setTargets (targets.concat (targetId));
        }
    };

    this.getMessage = function () {
        switch (this.getLogType()) {
            case logTypes.calculationAcceptanceChange:
                var msg = backswing.language.translate ('log_acceptance_changed') + ' '+ backswing.language.translate ('common_from'),
                    fromUsrs = [],
                    toUsrs = [],
                    usrRep = new UserRepository (Meteor.users);


                _.each (this.getFrom()['acceptedBy'], function (memberId) {
                    var usr = usrRep.findById (memberId);
                    if (usr) {
                        fromUsrs.push (usr.getName ())
                    }
                }, this);

                _.each (this.getTo()['acceptedBy'], function (memberId) {
                    var usr = usrRep.findById (memberId);

                    if (usr) {
                        toUsrs.push (usr.getName ())
                    }
                }, this);

                if (toUsrs.length === 0) {
                    toUsrs = [backswing.language.translate ('common_nobody')];
                }

                if (fromUsrs.length === 0) {
                    fromUsrs = [backswing.language.translate ('common_nobody')];
                }
                msg += ' ' + fromUsrs.join (', ') + ' ' + backswing.language.translate ('common_to') + ' ' + toUsrs.join (', ');

                return msg;


                break;
            default:
                return backswing.language.translate ('log_'+this.getLogType ());
                break;
        }
    };


};

ChangeLogRepository =  function (collection) {
    var _targetDummy = ChangeLogModel,
        _collection = collection
    ;

    /**
     * Adds entry for a new created calculation
     *
     * @param newCalc
     * @returns {*}
     */
    this.logCalculationCreated = function (newCalc) {
        check (newCalc, CalculationModel);
        var currentUserId = Meteor.userId ();

        if (Match.test (currentUserId, String)) {
            var logEntry = new ChangeLogModel ();
            logEntry.setUser (currentUserId);
            logEntry.setFrom ({});
            logEntry.setTo (newCalc);
            logEntry.addTarget (newCalc._id);
            logEntry.setLogType(logTypes.calculationCreated);

            return _collection.insert (logEntry);
        }

        return false;
    };

    this.logUpdateAcceptors = function (oldAcceptors, newAcceptors, calcId) {
        check (oldAcceptors, Array);
        check (newAcceptors, Array);
        check (calcId, String);


        var currentUserId = Meteor.userId ();
        if (Match.test (currentUserId, String)) {
            var logEntry = new ChangeLogModel ();
            logEntry.setUser (currentUserId);
            logEntry.setFrom ({'acceptedBy': oldAcceptors});
            logEntry.setTo ({'acceptedBy': newAcceptors});
            logEntry.addTarget (calcId);
            logEntry.setLogType(logTypes.calculationAcceptanceChange);

            return _collection.insert (logEntry);
        }
    };

    this.findByTarget = function (targetId) {
        check (targetId, String);
        return this.mapAll (_collection.find ({targets: {$in: [targetId]}}).fetch (), ChangeLogModel);
    };

    this.logCalculationClosed = function (calcId) {
        check (calcId, String);

        var currentUserId = Meteor.userId (),
            logEntry = new ChangeLogModel();

        if (Match.test (currentUserId, String)) {
            logEntry.setUser (currentUserId);
            logEntry.setFrom ({});
            logEntry.setTo ({});
            logEntry.addTarget (calcId);
            logEntry.setLogType(logTypes.calculationClosed);

            return _collection.insert (logEntry);

        }

    };

    this.logCalculationOpened = function (calcId) {

        var currentUserId = Meteor.userId (),
            logEntry = new ChangeLogModel();

        if (Match.test (currentUserId, String)) {
            logEntry.setUser (currentUserId);
            logEntry.setFrom ({});
            logEntry.setTo ({});
            logEntry.addTarget (calcId);
            logEntry.setLogType(logTypes.calculationOpened);

            return _collection.insert (logEntry);

        }
    };

    this.logCalculationDeleted = function (calcId, oldCalc) {
        check (calcId, String);
        check (oldCalc, String);

        var currentUserId = Meteor.userId (),
            logEntry = new ChangeLogModel();

        if (Match.test (currentUserId, String)) {
            logEntry.setUser (currentUserId);
            logEntry.setFrom (oldCalc);
            logEntry.setTo ({});
            logEntry.addTarget (calcId);
            logEntry.setLogType(logTypes.calculationDeleted);

            return _collection.insert (logEntry);

        }
    };

    /**
     *
     * @param calcId
     * @param {CalculationModel} oldCalc
     * @param {CalculationModel} newCalc
     */
    this.logGeneralCalculationChange = function (calcId, oldCalc, newCalc) {
        check (calcId, String);
        check (oldCalc, CalculationModel);
        check (newCalc, CalculationModel);

        var currentUserId = Meteor.userId (),
            logEntry = new ChangeLogModel();

        if (Match.test (currentUserId, String)) {
            logEntry.setUser (currentUserId);
            logEntry.setFrom (oldCalc);
            logEntry.setTo (newCalc);
            logEntry.addTarget (calcId);
            logEntry.setLogType (logTypes.calculationModified);

            return _collection.insert (logEntry);

        }
    }
};

if (Meteor.isServer) {


    Meteor.publish ('ChangeLog',

        /**
         *
         * @param {Array} targets
         * @param {int} limit
         * @returns {Cursor}
         */
        function (targets, limit) {
            var findSelector = {},
                findOptions = {sort: {createdAt: -1}};
            ;

            if(Match.test (targets, Array)) {
                findSelector.targets = {$in: targets};
            }

            if (Match.test(limit, Number)) {
                findOptions.limit = limit;
            }

            return ChangeLog.find (findSelector, findOptions);
    })
}

ChangeLogRepository.prototype = AbstractRepository.prototype;