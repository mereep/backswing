Calculations = new Mongo.Collection('calculations');

CalculationModel = function () {

    this._id = (new Mongo.ObjectID ()).valueOf();
    this.createdAt = new Date();
    this.lastModified = new Date();

    /**
     * is this calculation closed?
     */
    this.isClosed = false;

    /**
     * List of Items in this Calculations
     */
    this.items = [];

    /**
     * Which members accepted this calculations?
     */
    this.acceptedBy = [];

    /**
     * List of people that need to accept this list to
     * be able to close it
     * @type {Array}
     */
    this.needsAcceptanceFrom = [];
    /**
     * For which category type this one applies?
     * @type {string}
     */
    this.category = '';

    /**
     *
     * @type {string}
     */
    this.title = '';

    this.value = 0;

};

CalculationModel.prototype = {
    /**
     * closes a transaction or opens it
     * @param {boolean} isClosed
     */
    setIsClosed: function (isClosed) {
        check(isClosed, Boolean);
        this.isClosed = isClosed;
    },
    /**
     *
     * @param {Array} items
     */
    setItems: function (items) {
        check(items, Array);
        this.items = items;
    },

    /**
     * Sets the given value for this calculation
     * @param {Number} value
     */
    setValue: function (value) {
        check (value, Number);
        this.value = value;
    },

    getValue: function () {
        return this.value;
    },


    /**
     * Sums up all items of this calculation
     * that are one of categories
     *
     * @param {Array} categories
     */
    calculateItemValue: function (categories) {
        check (categories, Array);
        var items = this.getItems (),
            itemsObj = AbstractRepository.prototype.mapAll (items, CalculationItem),
            sum = 0
        ;

        /** @var {CalculationItem} obj*/
        _.each (itemsObj, function (obj) {
            if (Match.test (obj, CalculationItem) && _.contains (categories, obj.getCalculationCategory())) {
                sum += obj.getUnitPrice () * obj.getAmount ();
            }

        });

        return sum;
    },

    /**
     * Calculates the expected profit of this calculation by summing up
     * all items calculating agains calculations value
     *
     * @returns {number}
     */
    calculateProfit: function () {
        var allCats = _.keys (calculationCategoryTypes),
            itemSum = this.calculateItemValue (allCats)
        ;

        return this.getValue () - itemSum;

    },

    /**
     * Adds user that has to accept this calculation
     *
     * @param {string} userId
     */
    addNeedsAcceptanceFrom: function (userId) {
        check(userId, String);
        if(_.contains(this.addNeedsAcceptanceFrom(userId))) {
            this.needsAcceptanceFrom.concat(userId);
        }
    },

    /**
     * Sets the users that have to accept this
     * calculation
     *
     * @param users
     */
    setNeedsAccpetanceFrom: function (users) {
        check (users, Array);
        this.needsAcceptanceFrom = users;
    },
    /**
     * Gets all users this calculation needs to be accepted by
     *
     * @return Array
     */
    getNeedsAcceptanceFrom: function () {
        return this.needsAcceptanceFrom;
    },

    /**
     * Array userIds
     * @param {Array} userIds
     */
    setAcceptedBy: function (userIds) {
        check(userIds, Array);
        this.acceptedBy = userIds;
    },

    setCategory: function (category) {
      check (category, String);
        this.category = category;
    },

    setTitle: function (title) {
        check (title, String);
        this.title = title;
    },

    getTitle: function () {
        return this.title;
    },

    /**
     * @returns {CalculationModel.items|{}}
     */
    getItems: function () {
        return AbstractRepository.prototype.mapAll (this.items, CalculationItem);
    },

    getAcceptedBy: function () {
        return this.acceptedBy;
    },

    getCategory: function () {
        return this.category;
    },
    getLastModified: function () {
        return this.lastModified;
    },
    /**
     * Checks if a calculation is closed
     * return boolean
     */
    getIsClosed: function () {
        return this.isClosed;
    },

    /**
     *
     * @returns {Date|*}
     */
    getCreatedAt: function () {
        return this.createdAt;
    },


    /**
     * @returns {String}
     */
    getComment: function () {
       return this.comment;
    },

    isAcceptedByUser: function (userId) {
        check (userId, String);

        var acceptors = this.getAcceptedBy ();

        return _.contains (acceptors, userId);
    },

    /**
     * Removes user from acceptor list, if user is in list
     * otherwise does nothing
     *
     * @param userId
     */
    removeAcceptor: function (userId) {
        check (userId, String);
        if (this.isAcceptedByUser (userId)) {
            var acceptors = this.getAcceptedBy ();

            this.setAcceptedBy (_.filter (acceptors, function (acceptorId) {
                return userId !== acceptorId;
            }))
        }
    },


    /**
     * Adds an acceptor if not already accepted
     *
     * @param userId
     */
    addAcceptor: function (userId) {
        check (userId, String);

        if (!this.isAcceptedByUser (userId)) {
            this.acceptedBy.push (userId);
        }
    },

    /**
     *
     * @param userId
     * @return {boolean}
     */
    userNeedsToAccept: function (userId) {
        check (userId, String);

        return  _.contains (this.getNeedsAcceptanceFrom (), userId);
    },

    /**
     * Sets or unsets the user id from accepting this calculation
     * Checks if user needs to accept this calculation.
     *
     * @param userId
     * @return {boolean} true if state has been toggled, false otherwise
     */
    toggleAcceptance: function (userId) {
        check (userId, String);

        if (this.userNeedsToAccept (userId)) {
            if (this.isAcceptedByUser (userId)) {
                this.removeAcceptor (userId);
                return true;
            } else {
                this.addAcceptor (userId);
            }

            return true;
        }

        return false;
    },

    /**
     * Checks if this calculation can be closed
     * That means if all acceptors have accepted calculation
     * and calculation is not already closed
     *
     * @returns {boolean}
     */
    canBeClosed: function () {
        var acceptors = this.getAcceptedBy (),
            haveToAccept = this.getNeedsAcceptanceFrom (),
            canBeClosed = !this.getIsClosed ();

        _.each (haveToAccept, function (hasToAccept) {
            canBeClosed &= _.contains (acceptors, hasToAccept)
        });

        return canBeClosed;


    },

    /**
     * Checks if this calculation has acceptors that are not same as the current logged in user
     *
     * @returns {boolean}
     */
    isAcceptedByOthers: function () {
        var userId = Meteor.userId ();

        if (userId) {
            var acceptedByOthers = false;

            _.map (this.getAcceptedBy (), function (acceptor) {
               acceptedByOthers |= acceptor !== userId;
            });

            return acceptedByOthers;
        } else {
            //If we are not logged in we define it as accepted by others if someone accepted it
            return this.getAcceptedBy ().length >= 1;
        }
    }


};

CalculationRepository = function (collection) {
    var _collection = collection,
        _targetDummy = CalculationModel;


    /**
     * Adds a new calculation
     *
     * @param {CalculationModel} calc
     */
    this.addCalculation = function (calc) {
        check(calc, CalculationModel);
        var res = _collection.insert (calc);

        if (!res) {
            return false;
        } else {
            var clr = new ChangeLogRepository (ChangeLog);
            clr.logCalculationCreated (calc);
        }

        return true;

    };
    /**
     *
     * @param {int} amount
     */
    this.fillTo = function (amount) {
        check(amount, Match.Integer);
        var cnt = _collection.find().count(),
            diff = amount - cnt;

        if( diff > 0 ) {
            for(var i = 0; i < diff; i++) {
                var number =_.random(0, 4),
                    target = '';
                if (number === 0) {
                    target = targetCategoryTypes.asyl;
                }else if (number === 1) {
                    target = targetCategoryTypes.film;
                }else if (number === 2) {
                    target = targetCategoryTypes.golf;
                }else if (number === 3) {
                    target = targetCategoryTypes.other;
                } else {
                    target = targetCategoryTypes.restaurant;
                }
                var dummy = this.createForTarget (target);
                dummy.setTitle(Math.random().toString(36).substring(7));
                _collection.insert(dummy);
            }
        }
    },

    /**
     *
     * @param targetCategory
     * @returns {CalculationModel}
     */
    this.createForTarget = function (targetCategory){
        if (_.contains (targetCategoryTypes, targetCategory)) {
            var model = new _targetDummy();
            model.setCategory (targetCategory);
            return model;
        } else {
            throw "crateForTargetCategory should be called with a valid category. You called with" + targetCategory + '(Code: 2398213)';
        }
    };

    this.getOpenCalculationsCountByCategory = function (category)  {
        check(category, String);
        return _collection.find ({'isClosed': false, 'category': category}).count();
    };

    this.getCalculationsCountByCategory = function (category) {
        check(category, String);
        return _collection.find ({'category': category}).count();
    };

    this.getAllCalculationsByCategory = function (category) {
        check(category, String);
        var filters = {sort: {lastModified: -1}};

        return this.mapAll (_collection.find ({'category': category}, filters).fetch (), _targetDummy);
    };

    /**
     * @param {string} title
     *
     * @returns {CalculationModel}
     */
    this.findLatestByTitle = function (title) {
        check (title, String);
        var filters = {sort: {lastModified: -1}};

        return this.map (_collection.findOne ({'title': title}, filters), _targetDummy);
    };

    /**
     *
     * @param {string} id
     * @returns {CalculationModel}
     */
    this.findById = function (id) {
        check (id, String);
        return this.map (_collection.findOne ({'_id': id}), _targetDummy);
    };

    /**
     * Adds an item to a calculation.
     * Make sure you are allowed to do this before calling (This function just does adding)
     *
     * @param {string} calcId
     * @param {CalculationItem} item
     */
    this.addItemToCalculation = function (calcId, item) {
        check (calcId, String);
        check (item, CalculationItem);

        var calcRep = new CalculationRepository (Calculations),
            oldCalc = calcRep.findById (calcId);

        var ret =  _collection.update({'_id': calcId}, {$push: {items: item}, $set: {lastModified: new Date ()}});

        if(!ret) {
            return false;
        } else {
            var newCalc = calcRep.findById (calcId);

            if (Match.test (newCalc, CalculationModel) && Match.test (oldCalc, CalculationModel)) {
                var clr = new ChangeLogRepository (ChangeLog);
                clr.logGeneralCalculationChange (calcId, oldCalc, newCalc);
                return true;
            }

        }

    };

    /**
     * Finds the calculation Item in one of the calculations and
     * changes the specific one if found
     *
     * @param {CalculationItem} calculationItem
     * @return true if found and successfully updated, otherwise false
     */
    this.updateCalculationItem = function (calculationItem) {
        check (calculationItem, CalculationItem);

        //Check if calculation exists (may be unneeded)
        var calculation = this.map(_collection.findOne ({'items._id': calculationItem._id}), CalculationModel);

        if (calculation) {
            var found = true;
            calculation.setItems(
                _.map(calculation.items, function (value) {
                    if (value._id === calculationItem._id) {
                        value = calculationItem;
                    }
                    return value;
                }, this)
            );

            if (found) {
                return _collection.update({'items._id': calculationItem._id }, {
                    $set:
                        {
                            'items': calculation.getItems(),
                            'lastModified': new Date (),
                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    };

    /**
     * Removes a calculation item from the corresponding calculation
     *
     * @param {CalculationItem} calcItem
     * @returns {boolean}
     */
    this.deleteCalculationItem = function (calcItem) {
        check (calcItem, CalculationItem);

        var calculation = _collection.findOne ({'items._id': calcItem._id})
          ;


        if (calculation) {
            //Calculation found for this item. Seems legit
            calculation = this.map (calculation, CalculationModel);
            calculation.setItems(
                _.filter(calculation.getItems (), function (value) {
                    if (value._id === calcItem._id) {
                        return false;
                    }
                    return true;
                }, this)
            );

            var ret = _collection.update({'items._id': calcItem._id}, {$set: {'items': calculation.getItems (), 'lastModified': new Date ()}});

            if ( ret ) {
                var newCalc = calculation;

                if (Match.test (calculation, CalculationModel) && Match.test (newCalc, CalculationModel)) {
                    var clr = new ChangeLogRepository (ChangeLog);
                    clr.logGeneralCalculationChange (calculation._id, calculation, newCalc);
                    return true;
                }
            }


        }
        return false;
    };

    /**
     * Gets all items from this calc that have type set
     *
     * @param calcId
     * @param catType
     * @returns {Array}
     */
    this.getItemsForCategory = function (calcId, catType) {
        check (catType, String);

        var calc = this.map(_collection.findOne ({'_id': calcId}), _targetDummy),
            ret = []
        ;

        if ( ret ) {
            _.each (AbstractRepository.prototype.mapAll(calc.getItems (), CalculationItem), function (val, index) {
                if (val.getCalculationCategory() === catType) {
                    ret.push (val);
                }
            });
        }

        return ret;
    };

    this.updateComment = function (calculationId, comment) {
        check (calculationId, String);
        check (comment, String);

        return _collection.update (
            {'_id': calculationId},
            {
                $set: {
                    'comment': comment,
                    'lastModified': new Date ()
                }
            }
        );
    };

    this.updateValue = function (calculationId, value) {
        check (calculationId, String);
        check (value, Number);

        return _collection.update (
            {'_id': calculationId},
            {
                $set: {
                    'value': value,
                    'lastModified': new Date ()
                }
            }
        );
    };

    /**
     *
     * @param {string} calculationId
     * @param {Array} acceptors
     * @returns {boolean}
     */
    this.updateAcceptors = function (calculationId, acceptors) {
        check (calculationId, String);
        check (acceptors, Array);

        var oldAcceptors = this.map (_collection.findOne({_id: calculationId}), CalculationModel).getAcceptedBy ();

        var ret = _collection.update (
            {'_id': calculationId},

            {
                $set: {
                    'acceptedBy': acceptors,
                    'lastModified': new Date()
                }
            }
        );

        if(!ret) {
            return false;
        } else {
            var clr = new ChangeLogRepository (ChangeLog);
            clr.logUpdateAcceptors (oldAcceptors, acceptors, calculationId);
            return true;
        }
    };

    /**
     *
     * @param {string} calculationId
     * @returns {boolean}
     */
    this.openCalculation = function (calculationId) {
        check (calculationId, String);

        var ret = _collection.update (
            {'_id': calculationId},
            {
                $set :
                {
                    'isClosed': false,
                    'lastModified': new Date ()
                }
            }
        );

        if (! ret) {
            return false;
        } else {
            var clr = new ChangeLogRepository (ChangeLog);
            clr.logCalculationOpened (calculationId);

            return true;

        }
    };

    /**
     *
     * @param calcId
     * @returns {boolean}
     */
    this.closeCalculation = function (calcId) {
        check (calcId, String);

        var ret =  _collection.update (
            {'_id': calcId},
            {
                $set :
                {
                    'isClosed': true,
                    'lastModified': new Date ()
                }
            }
        );

        if (! ret) {
            return false;
        } else {
            var clr = new ChangeLogRepository (ChangeLog);
            clr.logCalculationClosed (calcId);

            return true;

        }
    };

    /**
     * Returns corresponding calculation for item-id
     *
     * @param {string} itemId
     * @returns {*}
     */
    this.getCalculationForItemId = function (itemId) {
        check (itemId, String);
        return this.map (_collection.findOne ({'items._id': itemId}), CalculationModel);

    };

    /**
     * Remove all acceptors
     * if keepSelf equals true, we keep our self
     *
     * @param {string} calcId
     * @param {boolean} keepSelf
     */
    this.removeAcceptors = function (calcId, keepSelf) {
        check (calcId, String);

        var calc = this.findById (calcId),
            oldAcceptors = calc.getAcceptedBy ();

        if (!keepSelf) {
            calc.setAcceptedBy ([]);
        } else {
            var userIdSelf = Meteor.userId ();
            calc.setAcceptedBy (
                _.filter (calc.getAcceptedBy (), function (acceptor) {
                    return (userIdSelf === acceptor);
                })
            );
        }

        var ret = _collection.update ({'_id': calcId}, {$set: {acceptedBy: calc.getAcceptedBy ()}});

        if (! ret) {
            return false;

        } else {
            var clr = new ChangeLogRepository (ChangeLog);
            clr.logUpdateAcceptors (oldAcceptors, calc.getAcceptedBy(), calcId);

            return true;

        }
    };

    /**
     *
     * @param {String} calcId
     * @returns {*}
     */
    this.deleteCalculation = function (calcId) {
        check (calcId, String);

        var ret = _collection.remove ({'_id': calcId});

        if (!ret) {
            return false;
        } else {
            var calcRep = new CalculationRepository (Calculations),
                calc = calcRep.findById (calcId);

            if (calc) {
                var clr = new ChangeLogRepository (ChangeLog);
                clr.logCalculationDeleted (calcId, calc);
            }

            return true;
        }
    };

    /**
     * Gets the date of the very first calculation
     * @returns {*|Date}
     */
    this.getFirstCalculationDate = function () {

        var ret = this.map (_collection.findOne ({}, {sort: {'createdAt':1}, limit: 1}), _targetDummy);

        return ret.getCreatedAt ();
    };

    /**
     * Gets the date of the latest calculation
     * @returns {*|Date}
     */
    this.getLatestCalculationDate = function () {

        var ret = this.map (_collection.findOne ({}, {sort: {'createdAt':-1}, limit: 1}), _targetDummy);
        return ret.getCreatedAt ();
    };


    /**
     * Define your own constraints
     *
     * @param  {object} constraints
     */
    this.findByConstraints = function (constraints) {
        check (constraints, Object);
        return AbstractRepository.prototype.mapAll(_collection.find (constraints, {sort: {createdAt: -1}}).fetch (), CalculationModel);

    };


};

CalculationRepository.prototype = AbstractRepository.prototype;