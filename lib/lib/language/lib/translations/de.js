/**
 * (C) 2015 Richard Vogel
 * E-Mail: webdes87@gmail.com
 *
 * Created 2015-11-23
 * This file belongs to the backswing meteor package
 *
 * German language file
 **/

langDE = {
    'title_login':              'einloggen',
    'title_home':               'Startseite',
    'title_overview':           'Übersicht',
    'title_create_calculation': 'Kalkulation erstellen',
    'title_show_calculation':   'Kalkulation bearbeiten',
    'title_statistics':         'Statistiken',


    'header_overview':          'Übersicht',
    'header_signin':            'Anmelden',
    'header_home':              'Startseite',
    'header_statistics':        'Statistiken',

    'common_opened':            'offen',
    'common_together':          'insgesamt',
    'common_last_change':       'Letzte Änderung',
    'common_name':              'Name',
    'common_date_complete':     'DD.MM.YYYY - HH:mm',
    'common_add_calculation':   'Kalkulation hinzufügen',
    'common_created_at':        'Erstellt am',
    'common_new_entry':         'Neuer Eintrag',
    'common_internal_error':    'Interner Fehler',
    'common_amount':            'Menge',
    'common_unit_price':        'Stückpreis',
    'common_title':             'Titel',
    'common_comment':           'Kommentar',
    'common_sum':               'Summe',
    'common_delete_entry':      'Eintrag löschen',
    'common_entries':           'Einträge',
    'common_to_top':            'Nach oben',
    'common_accepted':          'Akzeptiert',
    'common_not_accepted':      'Nicht akzeptiert',
    'common_closed':            'Geschlossen',
    'common_actions':           'Aktionen',
    'common_delete':            'Löschen',
    'common_are_you_sure':      'Sind Sie sicher?',
    'common_yes':               'Ja',
    'common_no':               'Nein',
    'common_from':              'von',
    'common_to':                'zu',
    'common_nobody':            'Niemand',
    'common_until':             'bis',
    'common_period':            'Zeitraum',
    'common_calculation_targets': 'Verrechungsorte',
    'common_results':            'Resultate',
    'common_hours':             'Stunden',
    'common_price_per_hour':     'Stundepreis',
    'common_no_rights':         'Für diese Aktion fehlen Ihnen die erforderlichen Rechte',


    'overview_really_delete':   'Möchten Sie diese Kalkulation wirklich unwiederbringlich löschen? Diese Aktion kann nicht widerrufen werden',

    'target_category_asyl':         'Asylheim',
    'target_category_golf':         'Golfclub',
    'target_category_restaurant':   'Restaurant',
    'target_category_other':        'Sonstige',
    'target_category_film':         'Filmclub',

    'calculation_accepted_by_others':   'Diese Kalkulation wurde bereits von Anderen akzeptiert, Änderungen an der Kalkulation führen zur Aufhebeung deren Akzeptanz',
    'calculation_is_closed':            'Diese Kalkulation ist geschlossen',
    'calculation_category_sourcing':    'Einkauf',
    'calculation_category_transport':   'KFZ / Transport',
    'calculation_category_staff':       'Personal',
    'calculation_category_facility':    'Einrichtung',
    'calculation_category_overhead':    'Betriebskosten / Gemeinskosten',
    'calculation_category_other':       'Sonstige',
    'calculation_acceptance_toggle_error': 'Fehler beim Akzeptieren / Ablehnen der Kalkulation',
    'calculation_toggle_acceptance_success': 'Akzeptanzstatus erfolgreich aktualisiert',
    'calculation_can_not_be_closed':        'Diese Kalkulation kann nicht geschlossen werden',
    'calculation_needed_reopen':        'Die Kalkulation musste erneut geöffnet werden',
    'calculation_close':            'Kalkulation schließen',
    'calcuation_add_item':              'Eintrag hinzufügen',
    'calculation_add_item_success':     'Eintrag erfolgreich hinzugefügt',
    'calculation_add_item_db_error':    'Fehler beim Einfügen des Eintrags in die Datenbank',
    'calculation_update_item_db_error': 'Fehler beim Editieren des Eintrags in der Datenbank',
    'calculation_update_item_success':   'Eintrag erfolgreich geändert',
    'calculation_delete_item_db_error': 'Fehler beim Löschen des Eintrags aus der Datenbank',
    'calculation_change_comment_success': 'Kommentar erfolgreich aktualisiert',
    'calculation_change_comment_db_error': 'Datenbankfehler beim ändern des Kommentares',
    'calculation_close_success':         'Kalkulation erfolgreich geschlossen',
    'calculation_close_db_error':       'Fehler beim aktualisieren der Datenbank',
    'overview_confirmed_by':            'Bestätigt von',
    'calculation_expected_profit':      'Erwarteter Gewinn',
    'calculation_value':                'Kalkulationswert',
    'calculation_change_value_db_error': 'Fehler beim Setzen des Kalkulationswertes in der Datenbank',
    'calculation_change_value_success': 'Kalkulationswert erfolgreich aktualisiert',

    'error_category_not_existent':      'Diese Kategorie existiert nicht. (Code 2109831290831)',
    'error_error':                      'Fehler',
    'error_needs_logged_in':            'Hierfür müssen Sie eingeloggt sein',
    'error_calculation_name_too_short': 'Der Name der Kalkulation muss mindestens 5 Zeichen lang sein',
    'error_calculation_responsibles_not_set':'Es muss mindestens ein Verantwortlicher deklariert werden',
    'error_calculation_add':            'Fehler beim erstellen der Kalkulation',

    'success_success':                  'Erfolgreich',
    'success_calculation_created':      'Kalkulation erfolgreich erstellt',

    'calculation_responsibles':         'Verantwortliche',
    'calculation_name':                 'Name der Kalkulation',

    'overview_calculation_delete_success': "Kalkulation erfolgreich gelöscht",
    'overview_calculation_delete_db_error': "Fehler entfernen der Kalkulation aus der Datenbank",
    'overview_no_calculations_for_category': 'Es sind keine Kalkulationen für diese Kategorie vorhanden',

    'log_calculation_created':          'Kalkulation erstellt',
    'log_acceptance_changed':           'Akzeptanzstatus geändert',
    'log_entries':                      'Logbuch',
    'log_calculation_closed':           'Kalkulation geschlossen',
    'log_calculation_deleted':          'Kalkulation gelöscht',
    'log_calculation_opened':           'Kalkulation geöffnet',
    'log_calculation_modified':         'Änderung vorgenommen',

    'statistics_only_show_closed_calcuations': 'Nur geschlossene anzeigen',
    'statistics_only_show_opened_calcuations': 'Nur offene anzeigen',
    'statistics_show_opened_and_closed_calculations': 'Offene und geschlossene anzeigen',
    'statistics_status_of_calculation':          'Kalkulationsstatus',
    'statistics_create':                         'Statistiken erstellen',
    'statistics_amount_of_calcs':                'Anzahl der Kalkulationen',
    'statistics_sum_of_calc_values':            'Kalkulationswerte',
    'statistics_sum_of_calc_items':             'Summe der Ausgaben',
    'statistics_calc_profit':                   'Gewinn',
    'statistics_expenses_by_categories':        'Ausgaben nach Kategorien'
};
