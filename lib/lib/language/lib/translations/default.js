/**
 * (C) 2015 Richard Vogel
 * E-Mail: webdes87@gmail.com
 *
 * Created 2015-11-23
 * This file belongs to the backswing meteor package
 *
 * Default language file
 **/

langDefault = {
        'appName':              'backswing',
        'title_login':          'login',
        'title_overview':       'Overview',
        'title_home':           'Home page',
        'header_overview':      'Overview',
        'header_signin':        'Sign in',
        'header_home':          'Home',

        'common_euro_symbol':   '€',

        'target_category_asyl':         'Refugees',
        'target_category_golf':         'Golf club',
        'target_category_restaurant':   'Restaurant',
        'target_category_other':        'Others',
        'target_category_film':         'Film club',

        'calculation_category_sourcing':    'Sourcing',
        'calculation_category_transport':   'Transport',
        'calculation_category_staff':       'Staff',
        'calculation_category_facility':    'Facility',
        'calculation_category_overhead':    'Operational costs',
        'calculation_category_other':       'Others',
};