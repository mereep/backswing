/**
 * (C) 2015 Richard Vogel
 * E-Mail: webdes87@gmail.com
 *
 * Created 2015-11-23
 * This file belongs to the backswing meteor package
 *
 * Helper for language dependent functions
 **/

Language = function() {
    var languageData = [];

    this.addLanguage = function  (key, data) {
        languageData[key] = data;
    };

    /**
     * Translates a string
     *
     * @param {string} key
     * @param {string} languageKey (if null we take user language)
     */
    this.translate = function(key, languageKey){
        if(typeof languageKey !== 'string'){
            languageKey = backswing.userHelper.getUserLanguage();
        }

        var foundLang = null;

        //Try to get string in requested language
        if(typeof languageData[languageKey] === 'object'){
            if(typeof languageData[languageKey][key] !== 'undefined'){
                foundLang = languageData[languageKey][key]
            }
        }

        if(!foundLang){
            //Try to fall back to common language
            if(typeof languageData['default'] === 'object'){
                if(typeof languageData['default'][key] !== 'undefined'){
                    foundLang = languageData['default'][key]
                }
            }
        }

        if(foundLang) {
            return foundLang;
        }else{
            return '';
        }

    }
};

Lang = new Language();
Lang.addLanguage('de', langDE);
Lang.addLanguage('default', langDefault);