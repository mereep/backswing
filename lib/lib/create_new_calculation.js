if(Meteor.isClient) {
    Meteor.subscribe('TargetCategory');
    Meteor.subscribe('AllUsers');
    Meteor.subscribe('Calculations');

    var targetCatRepo = new TargetCategoryRepository(window.TargetCategories),
        usersRepo = new UserRepository(Meteor.users),
        calculationRepository = new CalculationRepository(window.Calculations);
        ;


    var nameOfCat = undefined,
        id = undefined;

    Template.create_calculation.rendered = function (){

    };

    Template.create_calculation.helpers({
        users: function () {
            return _.filter(usersRepo.findAll(), function (user) {
                if (user.canBeResponsibleForCalculation()) {
                    return true;
                } else {
                    return false;
                }
            });
        },

        'getNameOfCategory': function () {
            var id = Router.current().params._id;
            var targetCat = targetCatRepo.findById(id);

            if(typeof targetCat === 'object') {
                nameOfCat = Lang.translate('target_category_'+targetCat.getLabelId());
            }

            return nameOfCat;
        },

        'getNameForUser': function (user) {
            return user.getName();
        },

        'getIdForUser': function (user) {
            return usersRepo.getIdFor(user);
        }
    });

    Template.create_calculation.events({
        'submit .create': function (event) {
            event.preventDefault ();
            var calculationName = event.target.calculationName.value,
                calculationResponsibles = $('#calculationResponsibles').val(),
                catId = Router.current ().params._id;
            ;

            Meteor.call('addCalculation', calculationName, calculationResponsibles, catId,function (err, res) {
                if (res.status === 'success') {
                    Notifier.success (res.msg, true);

                    //Route to new calculation view
                    //We first have to get the calculation we created now (having same timestamp as our object)
                    var calculation = calculationRepository.findLatestByTitle (calculationName);

                    if (Match.test (calculation, CalculationModel)) {
                        Router.go('/calculation/show/'+calculation._id);

                    } else {
                        Notifier.error(backswing.language.translate('error_calculation_add'), true);
                    }
                } else {
                    Notifier.error (res.msg, true);
                }
            });
        }
    });
}

if(Meteor.isServer) {

    var calculationRepository = new CalculationRepository(Calculations);
    var targetCatRepo  = new TargetCategoryRepository(TargetCategories);

    Meteor.methods({
        /**
         * Creates a new calculation
         *
         * @param calculationName
         * @param calculationResponsibles
         * @param {string} catId
         * @returns {{status: 'error|success', msg 'someMessage'}}
         */
        'addCalculation': function (calculationName, calculationResponsibles, catId) {
            if(Match.test(calculationName, String) && calculationName.length >= 5) {
                if (typeof calculationResponsibles === 'object' && calculationResponsibles.length >= 1) {
                    var calculation = new CalculationModel();
                    calculation.setTitle (calculationName);
                    calculation.setNeedsAccpetanceFrom (calculationResponsibles);

                    //Get category type
                    var targetCat = targetCatRepo.findById(catId),
                        targetCatType = targetCat.getType ();

                    calculation.setCategory (targetCatType);


                    if (calculationRepository.addCalculation(calculation)) {
                        return {status: 'success', msg: backswing.language.translate('success_calculation_created')};
                    } else {
                       var msg = backswing.language.translate('error_calculation_add');
                    }
                } else {
                   var msg = backswing.language.translate('error_calculation_responsibles_not_set');
                }
            } else {
                //Too short name
                var msg = backswing.language.translate('error_calculation_name_too_short');
            }

            return {'status': 'failure', 'msg': msg};
        }
    })
}