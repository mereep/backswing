/**
 * Created by birne on 23.12.15.
 */

if (Meteor.isClient) {
    Meteor.subscribe('CalculationCategories');

    var calcCatRep = new CalculationCategoryRepository(CalculationCategories),
        calcRep = new CalculationRepository (Calculations),
        calculation = undefined;


    Template.calculation.onCreated(function () {
        calculation = UI.getData();
        Meteor.subscribe ('ChangeLog', [calculation._id], 50);
    });

    Template.calculation.helpers({
        isCalculationNegative: function () {
            return this.calculateProfit () < 0;
        },
        isStaffItem: function (calcItem) {
            return calcItem.getType () === calculationItemTypes.staff;
        },
        getCalculationTitle: function () {
            return this.getTitle();
        },
        getCalculationProfit: function () {
            return this.calculateProfit ();
        },

        getCalculationValue: function () {
            return this.getValue ();
        },

        getCalculationCreatedDate: function () {
            var dateFormatString = backswing.language.translate('common_date_complete'),
                date = this.getCreatedAt();

            return moment(date).format(dateFormatString);
        },

        isCalculationClosed: function () {
            if (!Match.test (this, CalculationModel)) {
                var calculation = arguments[0];
            } else {
                var calculation = this;
            }
            return calculation.getIsClosed ();
        },

        calculationIsAcceptedByOthers: function () {
            return this.isAcceptedByOthers ();
        },

        getCurrentCalculation: function () {
            var calcRep = new CalculationRepository (Calculations),
                calc = calcRep.findById (calculation._id);

            return calc;
        },

        getCalculationItemType: function () {
            return this.getType();
        },

        calculationCanBeClosed: function () {
            return this.canBeClosed ();
        },

        getCalculationItemUnitPrice: function () {
            return this.getUnitPrice ();
        },

        getCalculationItemAmount: function () {
            return this.getAmount ();
        },

        getCalculationLastChange: function () {
            var dateFormatString = backswing.language.translate('common_date_complete'),
                date = this.getLastModified();

            return moment (date).format (dateFormatString);
        },

        getCalculationCategoryType: function () {
            return this.getType ();
        },

        getCalculationCategories: function () {
            return calcCatRep.getAllActive();
        },

        getCalculationCategoryLabel: function () {
            return this.getLabel ();
        },

        getLogEntries: function () {
            var clr = new ChangeLogRepository (ChangeLog),
                entries = clr.findByTarget (calculation._id);

            return entries;
        },

        getItemsForCategory: function () {
            var type = this.getType ()
            ;
            return calcRep.getItemsForCategory (calculation._id, type);
        },

        /**
         *
         * @param {CalculationCategoryModel} }calcCat
         * @returns {boolean}
         */
        userCanEditCalculationCategory: function (calcCat) {
            return backswing.userHelper.checkIfUserCanEditCalculationCategory (calcCat);
        },

        getCalculationComment: function () {
            return this.getComment ();
        },

        calculateItemSum: function () {
            return new Number(this.calculatePrice ()).toFixed(2);
        },

        getCalculationItemComment: function () {
            return this.getComment ();
        },

        countCalculationCategoryItems: function () {

            var type = this.getType ()
                ;
            return calcRep.getItemsForCategory (calculation._id, type).length;

        },
        getNeedsAcceptanceFrom: function () {
            return this.getNeedsAcceptanceFrom();
        },

        getCalculationCategoryItemsSum: function () {
            var type = this.getType (),
                items = calcRep.getItemsForCategory (calculation._id, type),
                sum = 0;

            _.each (items, function (item) {
                sum += item.calculatePrice() ;
            });

            return (new Number(sum).toFixed(2));
        },
        userHasAcceptedCalculation: function () {
            var userId = this,
                calc = calcRep.findById (calculation._id);

            return calc.isAcceptedByUser (userId.toString ());
        },
        /**
         * Get arrow down or up state css class
         *
         * @returns {*}
         */
        getCategoryOpenedIndicatorCss: function () {
            var id = this._id,
                status = Session.get ('calculation_show_status_'+id);

            //Opened -> down arrow
            if (status) {
                return 'fa-arrow-circle-down';
            } else {
                return 'fa-arrow-circle-right';
            }
        },

        isCategoryOpened: function () {
            return Session.get ('calculation_show_status_'+this._id);
        },

        /**
         * Checks if calculation is closed
         * @returns {boolean}
         */
        calculationIsClosed: function () {
            return this.getIsClosed ();
        },


    });

    Template.calculation.events ({

        'click .close-calculation': function () {
            Meteor.call ('closeCalculation', calculation._id, function (err, data) {
            if (!err) {
                Notifier.success (data, true);
            } else {
                Notifier.error (err.toString(), true);
            }
            });
        },

        'click .accept-calculation': function () {
            Meteor.call ('toggleAcceptanceOfCalculation', calculation._id, function (err, data) {
              if (!err) {
                  Notifier.success (data, true);
              } else {
                  Notifier.error (err.toString(), true);
              }
            });
        },

        /** Change Item **/
        'change input.title, change textarea.comment, change input.amount, change input.unit-price': function ($event) {

            /** @var CalculationItem item*/
            var item = this;

            var $target = $($event.currentTarget),
                $calculationItem = $target.parents ('.calculation-item').first (),
                amount = window.parseFloat($calculationItem.find ('input.amount').val ()),
                unitPrice = window.parseFloat($calculationItem.find ('input.unit-price').val ()),
                comment = $calculationItem.find ('textarea.comment').val (),
                title = $calculationItem.find ('input.title').val ()
            ;

            item.setTitle (title);
            item.setAmount (amount);
            item.setUnitPrice (unitPrice);
            item.setComment (comment);

            Meteor.call ('updateCalculationItem', item, function (err, data) {
                if (!err) {
                    Notifier.success (data, false);
                } else {
                    Notifier.error (err.toString(), true);
                }
            })

        },

        'click .btn.delete': function () {
            Meteor.call ('deleteCalculationItem', this, function (err, data) {
                if (!err) {
                    Notifier.success (data, false);
                } else {
                    Notifier.error (err.toString(), true);
                }
            });
        },

        'change .calculation-comment': function ($event) {
            var $target = $($event.currentTarget),
                comment = $target.val ()
            ;

            Meteor.call ('updateComment', this, comment, function (err, data) {
                if (!err) {
                    Notifier.success (data, false);
                } else {
                    Notifier.error (err.toString(), true);
                }
            });
        },

        'change .calculation-value': function ($event) {
            var $target = $($event.currentTarget),
                value = window.parseInt ($target.val ())
            ;


            Meteor.call ('updateValue', this, value, function (err, data) {
                if (!err) {
                    Notifier.success (data, false);
                } else {
                    Notifier.error (err.toString(), true);
                }
            });
        },

        /**
         * Toggle hide and show
         */
        'click .opened-indicator': function () {

            var currentState = Session.get('calculation_show_status_'+this._id);

            if (!currentState) {
                currentState = true;
            } else {
                currentState = false;
            }

            Session.setPersistent ('calculation_show_status_'+this._id, currentState);
        },

        /**
         * New Item
         */
        'click .add-item': function () {
            /**
             * @var CalculationCategoryModel category
             */
            var category = this
                ;

            //Build dummy item
            var item = new CalculationItem ();
            item.setTitle (backswing.language.translate('common_new_entry'));
            item.setAmount (1);
            item.setUnitPrice (0);
            item.setCalculationCategory (category.getType());

            if (category.getType() === calculationCategoryTypes.staff) {
                item.setType (calculationItemTypes.staff);
            } else {
                item.setType (calculationItemTypes.common);
            }

            //Ok, now we let the backend insert it
            Meteor.call ('addCalculationItem', calculation._id, item, function (err, data) {
                if (!err) {
                    Notifier.success (data, false);
                } else {
                    Notifier.error (err.toString(), true);
                }
            });

        }
    });
}


if (Meteor.isServer) {

    Meteor.publish('CalculationCategories', function () {
        return CalculationCategories.find ({});
    });


    Meteor.methods({

        /**
         * Tries to edit an item to the collection and checks if user is allowed to do that beforehand
         *
         * @param {CalculationItem} item
         */
        'updateCalculationItem': function (item) {
          if (typeof item === 'object') {
              if (backswing.userHelper.checkIfUserCanEditCalculationItem (item)) {
                  var calcRep = new CalculationRepository (Calculations),
                      abstractRep = new AbstractRepository(),
                      itemObj = abstractRep.map (item, CalculationItem),
                      calc = calcRep.getCalculationForItemId (item._id)
                  ;

                  if (!calc.getIsClosed ()) {
                      if (calcRep.updateCalculationItem (itemObj) ) {
                          if (calc.isAcceptedByOthers ()) {
                              calcRep.removeAcceptors (calc._id, true);
                          }
                          return backswing.language.translate ('calculation_update_item_success');
                      } else {
                          throw new Meteor.Error (5464554645, backswing.language.translate ('calculation_update_item_db_error'))
                      }
                  } else {
                      throw new Meteor.Error (996954567, backswing.language.translate ('calculation_is_closed'));
                  }
              } else {
                  throw new Meteor.Error (5675675656756, backswing.language.translate ('common_no_rights'));
              }
          } else {
              throw new Meteor.Error (78667867867786, backswing.language.translate ('common_internal_error'));
          }
        },

        /**
         * Tries to insert an item to the collection and checks if user is allowed to do that beforehand
         *
         * @param calculationId
         * @param {CalculationItem} calculationItem
         */
        'addCalculationItem': function (calculationId, calculationItem) {
            if (Match.test (calculationId, String) &&
                typeof calculationItem === 'object') {

                if (backswing.userHelper.checkIfUserCanEditCalculationItem (calculationItem)) {
                    var calcRep = new CalculationRepository (Calculations);
                    var abstractRep = new AbstractRepository(),
                        itemObj = abstractRep.map (calculationItem, CalculationItem),
                        calc = calcRep.findById (calculationId);

                    if (!calc.isClosed) {
                        if (calcRep.addItemToCalculation(calculationId, itemObj)) {
                            if (calc.isAcceptedByOthers ()) {
                                calcRep.removeAcceptors (calc._id, true);
                            }
                            return backswing.language.translate ('calculation_add_item_success');
                        } else {
                            throw new Meteor.Error (92834729384762, backswing.language.translate ('calculation_add_item_db_error'))
                        }
                    } else {
                        throw new Meteor.Error (2324534850349853094, backswing.language.translate ('calculation_is_closed'));
                    }
                } else {
                    throw new Meteor.Error (12312321312312, backswing.language.translate ('common_no_rights'))
                }
            } else {
                //No valid call bye, bye
                throw new Meteor.Error (2139871298378921, backswing.language.translate ('common_internal_error'));
            }
        },

        'deleteCalculationItem': function (calculationItem) {
            if (Match.test (calculationItem, Object)) {
                if (backswing.userHelper.checkIfUserCanEditCalculationItem(calculationItem)){
                    var calcRep = new CalculationRepository (Calculations);
                    var abstractRep = new AbstractRepository(),
                        itemObj = abstractRep.map (calculationItem, CalculationItem),
                        calc = calcRep.getCalculationForItemId (calculationItem._id)
                    ;

                    if (!calc.getIsClosed ()) {
                        if (calcRep.deleteCalculationItem (itemObj)) {
                            if (calc.isAcceptedByOthers ()) {
                                calcRep.removeAcceptors (calc._id, true);
                            }
                            return backswing.language.translate ('calculation_add_item_success');
                        } else {
                            throw new Meteor.Error (123908903890213890123, backswing.language.translate('calculation_delete_item_db_error'));
                        }
                    } else {
                        throw new Meteor.Error (84848548548, backswing.language.translate ('calculation_is_closed'));
                    }
                } else {
                    throw new Meteor.Error (90834590854098345, backswing.language.translate ('common_no_rights'))
                }
            } else {
                throw new Meteor.Error (9829038432908432098, backswing.language.translate ('common_internal_error'));

            }

        },

        'updateComment': function (calculation, newComment) {
            if (Match.test (calculation, Object) && Match.test (newComment, String)) {

                var calcRep = new CalculationRepository (Calculations),
                    calc = calcRep.findById (calculation._id);

                if (!calc.getIsClosed()) {
                    if (calcRep.updateComment (calculation._id, newComment)) {
                        if (calc.isAcceptedByOthers ()) {
                            calcRep.removeAcceptors (calc._id, true);
                        }
                        return backswing.language.translate ('calculation_change_comment_success');
                    } else {
                        throw new Meteor.Error (231235555, backswing.language.translate('calculation_change_comment_db_error'));
                    }
                } else {
                    throw new Meteor.Error (89438594343987, backswing.language.translate ('calculation_is_closed'));
                }
            } else {
                throw new Meteor.Error (349503493049, backswing.language.translate ('common_internal_error'));

            }
        },

        'updateValue': function (calculation, value) {

            if (Match.test (calculation, Object) && Match.test (value, Number)) {
                var calcRep = new CalculationRepository (Calculations),
                    calc = calcRep.findById (calculation._id);

                if (!calc.getIsClosed()) {
                    if (calcRep.updateValue (calculation._id, value)) {
                        if (calc.isAcceptedByOthers ()) {
                            calcRep.removeAcceptors (calc._id, true);
                        }
                        return backswing.language.translate ('calculation_change_value_success');
                    } else {
                        throw new Meteor.Error (564554465645, backswing.language.translate('calculation_change_value_db_error'));
                    }
                } else {
                    throw new Meteor.Error (45645456456546, backswing.language.translate ('calculation_is_closed'));
                }
            } else {
                throw new Meteor.Error (345345345345345, backswing.language.translate ('common_internal_error'));

            }
        },

        'toggleAcceptanceOfCalculation': function (calculationId) {
            if (Match.test (calculationId, String)) {
                var calcRep = new CalculationRepository (Calculations),
                    /** @var {CalculationModel} calc */
                    calc = calcRep.findById (calculationId);

                if (Match.test (calc, CalculationModel)) {
                    if (calc.toggleAcceptance (Meteor.userId ()) && calcRep.updateAcceptors (calc._id, calc.getAcceptedBy ())) {
                        var resString = backswing.language.translate ('calculation_toggle_acceptance_success');

                        //If calculation was opened, and we switched to not accepted we have to close calculation
                        if (calc.getIsClosed() && !calc.isAcceptedByUser (Meteor.userId ())) {
                            calcRep.openCalculation (calculationId);
                            resString += "<br /> "+
                                backswing.language.translate ('calculation_needed_reopen');
                        }

                        return resString;

                    } else {
                        throw new Meteor.Error (231235555, backswing.language.translate('calculation_acceptance_toggle_error'));
                    }
                } else {
                    throw new Meteor.Error (8585732432, backswing.language.translate ('common_internal_error'));
                }
            } else {
                throw new Meteor.Error (324233246, backswing.language.translate ('common_internal_error'));

            }
        },

        'closeCalculation': function (calcId) {
            if (Match.test (calcId, String)) {
                var calcRep = new CalculationRepository (Calculations),
                    /** @var {CalculationModel} calc */
                    calc = calcRep.findById (calcId);

                if (Match.test (calc, CalculationModel)) {
                    if (calc.canBeClosed ()) {
                        if (calcRep.closeCalculation (calcId)) {
                            return backswing.language.translate ('calculation_close_success');
                        } else {
                            throw new Meteor.Error (534345345, backswing.language.translate ('calculation_close_db_error'));
                        }
                    } else {
                        throw new Meteor.Error (4564564564563, backswing.language.translate ('calculation_can_not_be_closed'));
                    }
                } else {
                    throw new Meteor.Error (53493453498543, backswing.language.translate ('common_internal_error'));
                }
            } else {
                throw new Meteor.Error (23423423423, backswing.language.translate ('common_internal_error'));

            }
        }
    });

}