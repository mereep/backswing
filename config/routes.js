/**
 * (C) 2015 Richard Vogel
 * E-Mail: webdes87@gmail.com
 *
 * Created 2015-11-23
 * This file belongs to the backswing meteor package
 *
 * Here we define routes for the package
 **/

var loginRoutes = ['overview', 'calculation/:_id', 'calculation/add/:_id', 'calculation/show/:id', 'statistics'];
var adminRoutes = ['calculation/add/:_id'];

Router.configure({
    layoutTemplate: 'MainLayout'
});

Router.route('/', function () {
    this.name = 'home';

    this.render('MainHeader', {to: 'header'});
    this.render('MainFooter', {to: 'footer'});
    this.render('Home')
});

Router.route('/overview', function () {
        this.name = 'overview';


        this.render('MainHeader', {to: 'header'});
        this.render('MainFooter', {to: 'footer'});
        this.render('overview')
    }
);

Router.route('/calculation/show/:_id', {
    name: 'calculation',
    loadingTemplate: 'loading',

    waitOn: function () {
        return Meteor.subscribe('Calculations');
    },

    data: function () {
        var id = this.params._id,
            calculationRep = new CalculationRepository(Calculations)
            ;

        return calculationRep.findById (id);
    },

    action: function (){
        var id = this.params._id,
            calculationRep = new CalculationRepository(Calculations)
            ;

        var calc = calculationRep.findById (id);

        if (Match.test (calc, CalculationModel)) {
            this.render('MainHeader', {to: 'header'});
            this.render('MainFooter', {to: 'footer'});
            this.render('calculation');
        } else {
            Notifier.error(backswing.language.translate('error_category_not_existent'), true);
            this.redirect ('/');
        };

    }

});

Router.route('/statistics',  {
    name: 'statistics',
    loadingTemplate: 'loading',

    waitOn: function () {
        return Meteor.subscribe('Calculations');
    },

    action: function () {
        this.render('MainHeader', {to: 'header'});
        this.render('MainFooter', {to: 'footer'});
        this.render('statistics');
    }
});

Router.route('calculation/add/:_id', {

    loadingTemplate: 'loading',

    waitOn: function() {
        return Meteor.subscribe('TargetCategories');
    },


    action: function () {
        var id = this.params._id,
            targetCatRep = new TargetCategoryRepository(TargetCategories)
            ;

        var targetCat = targetCatRep.findById(id);

        if (typeof targetCat === 'object') {
            this.render('MainHeader', {to: 'header'});
            this.render('MainFooter', {to: 'footer'});
            this.render('create_calculation');
            Session.set('CreateCalculationForTargetCategory', id);

        } else {
            Notifier.error(backswing.language.translate('error_category_not_existent'), true);
            Meteor.Error(backswing.language.translate('error_category_not_existent'));

            this.redirect('/');
        }
    }



});

Router.onBeforeAction(function () {
    // all properties available in the route function
    // are also available here such as this.params

    console.log (this);
    if (!Meteor.userId()) {
        // if the user is not logged in, render the Login template
        Notifier.error(backswing.language.translate('error_needs_logged_in'), true);
        this.redirect('/');
    } else {
        // otherwise don't hold up the rest of hooks or our route/action function
        // from running
        this.next();
    }
}, {
    'only': loginRoutes
});